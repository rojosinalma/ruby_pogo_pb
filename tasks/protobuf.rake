namespace :protobuf do
  desc 'Compiles protos with Ruby'
  task :compile do
    cd('lib/ruby_pogo_pb/POGOProtos')
    system 'python compile_single.py -l ruby -o ../'
  end
end
