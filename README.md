Ruby Pokemon Go Protobuf
========================

This repository contains the Ruby compiled [ProtoBuf](https://github.com/google/protobuf) files needed to decode the PokémonGo RPC.

# Warning!

Misuse of the files here and Niantic's API can lead to bans within Pokemon Go.

This project's purpose is just for fun and learning. I don't encourage anybody doing anything that goes against [Pokemon Go ToS](https://www.nianticlabs.com/terms/pokemongo/en).

# Usage

TODO

# Compile for youself

TODO

# Credits

This project gets the proto files from [AeonLucid's](https://github.com/AeonLucid/POGOProtos) project.

Thanks to him and all others that are doing really interesting stuff with Pokemon Go.
